#ifndef __MESAMONGOOSE_DESPACHANTE__
#define __MESAMONGOOSE_DESPACHANTE__

#include "mongoose.h"

void *eventHandler(enum mg_event event,
	    struct mg_connection *conn,
		const struct mg_request_info *request);

#endif


