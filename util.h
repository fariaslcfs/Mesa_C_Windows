#ifndef __MESAMONGOOSE_UTIL__
#define __MESAMONGOOSE_UTIL__

#include "mesamongoose.h"
#include "mongoose.h"

extern const char *jsonReplyStart;
extern const char *javascriptReplyStart;
extern const char *txtReplyStart;

char *meustrcpy(char *target, const char *source, size_t len);
void sha256(char *string, char outputBuffer[65]); 
void getBufferParametros(struct mg_connection *conn,
				const struct mg_request_info *ri,
				char *dst, size_t dst_len); 
int codifica(char *fonte, int tamFonte,
             char *destino, int tamDestino); 

void printVerbose(char *s);
#endif


