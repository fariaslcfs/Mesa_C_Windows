#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <modbus.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

#include "mesa.h"
#include "util.h"

extern const char *jsonReplyStart;

clock_t start, end;

int stop;
int rec;

uint32_t tmout;
uint32_t counter;
double time_elapsed;
FILE * fp = NULL;

float accazi, acctilt, velposazi, velpostilt;
float VPosAzi, APosAzi, MaxVelAzi, MaxAccAzi;
float VPosTilt, APosTilt, MaxVelTilt, MaxAccTilt;

typedef struct dWord_ {
	union {
		uint32_t val;
		int32_t valsigned;
		struct {
			uint16_t val0;
			uint16_t val1;
		};
	};
} dWord;

static void escrevereg(float valor, int endereco) {
	modbus_t *ctx;

	ctx = modbus_new_tcp(SERVER, PORT);
	///modbus_set_debug(ctx, TRUE);

	if (modbus_connect(ctx) == -1) {
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
	}

	modbus_write_register(ctx, endereco, valor);

	modbus_close(ctx);
	modbus_free(ctx);
}

static void escreveregs(int32_t valor, int16_t endereco) {

	modbus_t *ctx;

	uint16_t regs[2];

	//Split
	regs[0] = valor & 0x0000FFFF;
	regs[1] = valor >> 16;

	ctx = modbus_new_tcp(SERVER, PORT);
	//modbus_set_debug(ctx, TRUE);

	if (modbus_connect(ctx) == -1) {
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
	}

	modbus_write_registers(ctx, endereco, 1, &regs[0]);

	modbus_write_registers(ctx, endereco + 1, 1, &regs[1]);

	modbus_close(ctx);
	modbus_free(ctx);
}

char *mesaDados(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	double azimutePosicaoAtual;
	double azimuteVelocidadeAtual;
	double tiltPosicaoAtual;
	double tiltVelocidadeAtual;

	modbus_t *ctx;
	dWord val;

	double temp;

	char *buffer[tamData];

	mg_printf(conn, "%s", jsonReplyStart);

	ctx = modbus_new_tcp(SERVER, PORT);
	//modbus_set_debug(ctx, TRUE);

	if (modbus_connect(ctx) == -1) {
		sprintf(buffer, "{ \"erro\" : \"%s\" }", modbus_strerror(errno));
		mg_write(conn, buffer, strlen(buffer));
		modbus_free(ctx);
		return "1";
	}

	// leitura de posicao atual de azimute
	modbus_read_input_registers(ctx, 0, 1, &val.val0);
	modbus_read_input_registers(ctx, 1, 1, &val.val1);
	temp = val.val;
	// azimutePosicaoAtual = (temp < 0.0 ? (temp + 360.0) : temp) / 1000000.0;
	azimutePosicaoAtual = temp / 1000000.0;

	// leitura de velocidade atual de azimute
	modbus_read_input_registers(ctx, 2, 1, &val.val0);

	modbus_read_input_registers(ctx, 3, 1, &val.val1);
	azimuteVelocidadeAtual = val.valsigned / 1000000.0;

	// leitura de posicao atual de tilt
	modbus_read_input_registers(ctx, 4, 1, &val.val0);
	modbus_read_input_registers(ctx, 5, 1, &val.val1);
	temp = val.val;
	// tiltPosicaoAtual = (temp < 0.0 ? (temp + 360.0) : temp) / 1000000.0;
	tiltPosicaoAtual = temp / 1000000.0;

	// leitura de velocidade atual de tilt
	modbus_read_input_registers(ctx, 6, 1, &val.val0);
	modbus_read_input_registers(ctx, 7, 1, &val.val1);
	tiltVelocidadeAtual = val.valsigned / 1000000.0;

	if (rec == 1) {
		fprintf(fp, "%ld\t%.2f\t%.2f\t%.2f\t%.2f\n", counter, azimutePosicaoAtual, azimuteVelocidadeAtual,
				tiltPosicaoAtual, tiltVelocidadeAtual);
		counter++;
	}

	sprintf(buffer, "{"
			"\"azimutePosicaoLida\":\"%.2f\","
			"\"azimuteVelocidadeLida\":\"%.2f\","
			"\"tiltPosicaoLida\":\"%.2f\","
			"\"tiltVelocidadeLida\":\"%.2f\"}", azimutePosicaoAtual,
			azimuteVelocidadeAtual, tiltPosicaoAtual, tiltVelocidadeAtual);

	mg_write(conn, buffer, strlen(buffer));

	modbus_close(ctx);
	modbus_free(ctx);

	return "1";
}

char *executaParar(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	stop = 1;

	char buffer[tamData];

	escrevereg(1, 22);
	escrevereg(1, 24);

	// Tilt
	escrevereg(0, 26);
	escrevereg(10000, 40);
	escrevereg(10000, 42);
	escreveregs(0, 46);

	// Azimute
	escrevereg(0, 28);
	escrevereg(10000, 50);
	escrevereg(10000, 52);
	escreveregs(0, 56);

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *executaPararGravacao(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	rec = 0; // usado para a indicação visual (círculo vermelho) na interface web
	counter = 0;

	end = clock();
	double time_elapsed = ((double) (end - start)) / CLOCKS_PER_SEC / 60;
	fprintf(fp, "\nTempo total de gravação ~= %.2f min\n", time_elapsed);

	if (fp != NULL)	fclose(fp);

	char buffer[tamData];

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *executaEmergencia(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	stop = 1;

	char buffer[tamData];

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	escreveregs(4, 20);
	sleep(1);
	escreveregs(0, 20);
	sleep(1);

	return "1";
}

char *executaReset(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	stop = 1;

	char buffer[tamData];

	escrevereg(1, 20);
	sleep(1);
	escrevereg(0, 20);
	sleep(1);

	escrevereg(1, 20);
	sleep(1);
	escrevereg(0, 20);
	sleep(1);

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *executaPosicionamento(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	stop = 1;

	char buffer[tamData];
	char posAzimute[tamData];
	char posTilt[tamData];
	float pAzimute, pTilt;

	mg_get_var(data, tamData, "posAzimute", posAzimute, sizeof(posAzimute) - 1);
	mg_get_var(data, tamData, "posTilt", posTilt, sizeof(posTilt) - 1);

	pAzimute = atof(posAzimute) * 1000;
	pTilt = atof(posTilt) * 1000;

	if (pAzimute < 0) {
		pAzimute = 360000 - abs(pAzimute);
	}

	if (pTilt < 0) {
		pTilt = 360000 - abs(pTilt);
	}

	// Tilt
	escrevereg(2, 22);
	escrevereg(0, 26);
	escreveregs(pTilt, 60);
	escrevereg(APosTilt, 40);
	escrevereg(MaxAccTilt, 42);
	escrevereg(VPosTilt, 62);
	//sleep(1);
	escrevereg(0, 26);
	//sleep(1);
	escrevereg(4, 26);

	// Azimuth
	escrevereg(2, 24);
	escrevereg(0, 28);
	escreveregs(pAzimute, 70);
	escrevereg(APosAzi, 50);
	escrevereg(MaxAccAzi, 52);
	escrevereg(VPosAzi, 72);
	//sleep(1);
	escrevereg(0, 28);
	//sleep(1);
	escrevereg(4, 28);

	fprintf(stdout, "executaPosicionamento %s %s\n", posAzimute, posTilt);

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *executaJog(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	stop = 1;

	char buffer[tamData];
	char velAzimute[tamData];
	char velTilt[tamData];

	float vAzimute;
	float vTilt;

	mg_get_var(data, tamData, "velAzimute", velAzimute, sizeof(velAzimute) - 1);
	mg_get_var(data, tamData, "velTilt", velTilt, sizeof(velTilt) - 1);

	vAzimute = atof(velAzimute) * 100.0;
	vTilt = atof(velTilt) * 100.0;

	if (abs(vAzimute) > 50000.0) {
		if (vAzimute < 0.0) {
			vAzimute = -50000.0;
		} else {
			vAzimute = 50000.0;
		}
	}

	if (abs(vTilt) > 20000.0) {
		if (vTilt < 0.0) {
			vTilt = -20000.0;
		} else {
			vTilt = 20000.0;
		}
	}

	escrevereg(1, 22);
	escrevereg(1, 24);

	escrevereg(0, 26);
	escrevereg(0, 28);

	// Azimute
	if (vAzimute < 0.0) {
		escreveregs(abs(vAzimute), 56);
		escrevereg(2, 28);      //reverso
	} else if (vAzimute > 0.0) {
		escreveregs(vAzimute, 56);
		escrevereg(1, 28);      //avante
	} else {
		escrevereg(0, 28);
	}

	// Tilt
	if (vTilt < 0.0) {
		escreveregs(abs(vTilt), 46);
		escrevereg(2, 26);      //reverso
	} else if (vTilt > 0.0) {
		escreveregs(vTilt, 46);
		escrevereg(1, 26);     //avante
	} else {
		escrevereg(0, 26);
	}

	fprintf(stdout, "executaJog %s %s\n", velAzimute, velTilt);

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *executaJogAutomatizada(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	stop = 0;

	char buffer[tamData];
	char inctime[tamData];
	char incveltilt[tamData];
	char incvelazi[tamData];
	char velinitilt[tamData];
	char velendtilt[tamData];
	char veliniazi[tamData];
	char velendazi[tamData];

	int t;
	float accveltilt;
	float accvelazi;
	float limittiltloop;
	float limitaziloop;
	float incVTilt;
	float incVAzi;
	float vAzimute, vTilt;
	float vTiltShow, vAziShow;
	int totalTime;

	strcpy(buffer, "\"ok\"");

	mg_get_var(data, tamData, "inctime", inctime, sizeof(inctime) - 1);
	mg_get_var(data, tamData, "incveltilt", incveltilt, sizeof(incveltilt) - 1);
	mg_get_var(data, tamData, "incvelazi", incvelazi, sizeof(incvelazi) - 1);
	mg_get_var(data, tamData, "velinitilt", velinitilt, sizeof(velinitilt) - 1);
	mg_get_var(data, tamData, "velendtilt", velendtilt, sizeof(velendtilt) - 1);
	mg_get_var(data, tamData, "veliniazi", veliniazi, sizeof(veliniazi) - 1);
	mg_get_var(data, tamData, "velendazi", velendazi, sizeof(velendazi) - 1);

	t = atoi(inctime);
	accveltilt = atof(velinitilt);
	accvelazi = atof(veliniazi);
	limittiltloop = abs(atof(velendtilt));
	limitaziloop = abs(atof(velendazi));
	incVTilt = atof(incveltilt);
	incVAzi = atof(incvelazi);

	totalTime =
			(limittiltloop >= limitaziloop) ?
					abs((limittiltloop / incVTilt * t) + 6) :
					abs((limitaziloop / incVAzi * t) + 6);

	fprintf(stdout,
			"\nVlimitTil %s graus/s\nVlimitAzimute %s graus/s\nIncTilt %s graus\nIncAzi %s graus\nTime %ss\nTempo Total %ds + tempo de reposicionamento para 0 tilt e 0 azimute\n\n",
			velendtilt, velendazi, incveltilt, incvelazi, inctime, totalTime);

	// necessarios - tirados do executaPosicionamento

	escrevereg(1, 22);
	escrevereg(1, 24);
	escrevereg(10000, 40);
	escrevereg(10000, 42);
	escrevereg(10000, 50);
	escrevereg(10000, 52);

	while (((abs(accveltilt) < limittiltloop) || (abs(accvelazi) < limitaziloop))
			&& !stop) {

		accveltilt = accveltilt + incVAzi;
		if (abs(accveltilt) >= limittiltloop) {
			/*incVTilt = 0.0;*/
			accveltilt = atof(velendtilt);
		}

		accvelazi = accvelazi + incVTilt;
		if (abs(accvelazi) >= limitaziloop) {
			/*incVAzi = 0.0;*/
			accvelazi = atof(velendazi);
		}

		vAzimute = accvelazi * 100.0;
		vTilt = accveltilt * 100.0;

		// Definição de limite de velocidade máxima de azimute
		if (abs(vAzimute) > 50000.0) {
			if (vAzimute < 0.0) {
				vAzimute = -50000.0;
			} else {
				vAzimute = 50000.0;
			}
		}

		// Definição de limite de velocidade máxima de tilt
		if (abs(vTilt) > 20000.0) {
			if (vTilt < 0.0) {
				vTilt = -20000.0;
			} else {
				vTilt = 20000.0;
			}
		}

		// Tilt
		if (vTilt < 0.0) {
			escreveregs(abs(vTilt), 46);
			escrevereg(2, 26);      //reverso
		} else if (vTilt > 0.0) {
			escreveregs(vTilt, 46);
			escrevereg(1, 26);     //avante
		} else {
			escrevereg(0, 26);
		}

		// Azimute
		if (vAzimute < 0.0) {
			escreveregs(abs(vAzimute), 56);
			escrevereg(2, 28);      //reverso
		} else if (vAzimute > 0.0) {
			escreveregs(vAzimute, 56);
			escrevereg(1, 28);      //avante
		} else {
			escrevereg(0, 28);
		}

		mg_printf(conn, "%s", jsonReplyStart);
		mg_write(conn, buffer, strlen(buffer));

		incVTilt < 0 ? (vTiltShow = vTilt) : (vTiltShow = -vTilt);
		incVAzi < 0 ? (vAziShow = vAzimute) : (vAziShow = -vAzimute);

		fprintf(stdout,
				"Velocidades -->  Azimute: %.2f grau(s)/s Tilt: %.2f grau(s)/s\n",
				vTiltShow / 100, vAziShow / 100);

		sleep(t);
	}

	// Espera pra não dar tranco.

	// sleep(10);

	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	/*
	 // Reposicionamento para 0 tilt e 0 azimute

	 // Tilt
	 escrevereg(2, 22);
	 escrevereg(0, 26);
	 escreveregs(0, 60);  // Valor 0 no registrador 60 - vai pra posição inicial 0
	 escrevereg(10000, 64);
	 escrevereg(10000, 66);
	 escrevereg(3000, 62);
	 sleep(1);
	 escrevereg(0, 26);
	 sleep(1);
	 escrevereg(4, 26);

	 sleep(2);

	 // Azimuth
	 escrevereg(2, 24);
	 escreveregs(0, 70);  // Valor 0 no registrador 70 - vai pra posição inicial 0
	 escrevereg(10000, 50);
	 escrevereg(10000, 52);
	 escrevereg(3000, 56);
	 sleep(1);
	 escrevereg(0, 28);
	 sleep(1);
	 escrevereg(4, 28);

	 */

	return "1";
}

char *executaHabJog(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	stop = 1;

	char buffer[tamData];
	char velPosAziHab[tamData];
	char velPosTiltHab[tamData];

	float vAzimute;
	float vTilt;

	mg_get_var(data, tamData, "velPosAziHab", velPosAziHab, sizeof(velPosAziHab) - 1);
	mg_get_var(data, tamData, "velPosTiltHab", velPosTiltHab, sizeof(velPosTiltHab) - 1);

	vAzimute = abs(atof(velPosAziHab) * 100.0);
	vTilt = abs(atof(velPosTiltHab) * 100.0);

	if (vAzimute > 50000.0) {
		vAzimute = 50000.0;
	}

	if (vTilt > 20000.0) {
		vTilt = 20000.0;
	}

	escreveregs(vAzimute, 56);
	escrevereg(0, 24);
	escreveregs(vTilt, 46);
	escrevereg(0, 22);

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *executaDesJog() {

	stop = 1;

	/*	double velAziConf = leInputRegs(72);
	 double velTiltConf = leInputRegs(62);

	 escreveregs(0.0, 56);
	 escreveregs(0.0, 46);*/

//azimute
	escrevereg(2, 24);

//sleep(1);

	//tilt
	escrevereg(2, 22);

//fprintf(stdout, "%s", "Jog Mode Desabilitado");

	return "1";
}

char *setConfig(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char buffer[tamData];
	char velPosAzimute[tamData];
	char accPosAzimute[tamData];
	char maxVelAzimute[tamData];
	char maxAccAzimute[tamData];
	char velPosTilt[tamData];
	char accPosTilt[tamData];
	char maxVelTilt[tamData];
	char maxAccTilt[tamData];

	mg_get_var(data, tamData, "velPosAzimute", velPosAzimute,
			sizeof(velPosAzimute) - 1);
	mg_get_var(data, tamData, "accPosAzimute", accPosAzimute,
			sizeof(accPosAzimute) - 1);
	mg_get_var(data, tamData, "maxVelAzimute", maxVelAzimute,
			sizeof(maxVelAzimute) - 1);
	mg_get_var(data, tamData, "maxAccAzimute", maxAccAzimute,
			sizeof(maxAccAzimute) - 1);
	mg_get_var(data, tamData, "velPosTilt", velPosTilt, sizeof(velPosTilt) - 1);
	mg_get_var(data, tamData, "accPosTilt", accPosTilt, sizeof(accPosTilt) - 1);
	mg_get_var(data, tamData, "maxVelTilt", maxVelTilt, sizeof(maxVelTilt) - 1);
	mg_get_var(data, tamData, "maxAccTilt", maxAccTilt, sizeof(maxAccTilt) - 1);

	VPosAzi = atof(velPosAzimute) * 100.0;
	APosAzi = atof(accPosAzimute) * 100.0;
	MaxVelAzi = atof(maxVelAzimute) * 100.0;
	MaxAccAzi = atof(maxAccAzimute) * 100.0;

	VPosTilt = atof(velPosTilt) * 100.0;
	APosTilt = atof(accPosTilt) * 100.0;
	MaxVelTilt = atof(maxVelTilt) * 100.0;
	MaxAccTilt = atof(maxAccTilt) * 100.0;

	if (VPosAzi > MaxVelAzi)
		VPosAzi = MaxVelAzi;
	if (APosAzi > MaxAccAzi)
		APosAzi = MaxAccAzi;

	if (VPosTilt > MaxVelTilt)
		VPosTilt = MaxVelTilt;
	if (APosTilt > MaxAccTilt)
		APosTilt = MaxAccTilt;

// Azimute
	escreveregs(VPosAzi, 72);
	escreveregs(APosAzi, 74);
	escreveregs(APosAzi, 76);
	escreveregs(APosAzi, 50);
	escreveregs(APosAzi, 52);
	escreveregs(MaxVelAzi, 90);
	escreveregs(MaxAccAzi, 92);

// Tilt
	escreveregs(VPosTilt, 62);
	escreveregs(APosTilt, 64);
	escreveregs(APosTilt, 66);
	escreveregs(APosTilt, 40);
	escreveregs(APosTilt, 42);
	escreveregs(MaxVelTilt, 80);
	escreveregs(MaxAccTilt, 82);

	escrevereg(2, 20);
	sleep(1);
	escrevereg(0, 20);

	fprintf(stdout, "setConfig  %s %s %s %s %s %s %s %s\n", velPosAzimute,
			accPosAzimute, maxVelAzimute, maxAccAzimute, velPosTilt, accPosTilt,
			maxVelTilt, maxAccTilt);

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *getConfig(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	modbus_t *ctx;
	dWord val;
	double velPosAzimute;
	double accPosAzimute;
	double maxVelAzimute;
	double maxAccAzimute;
	double velPosTilt;
	double accPosTilt;
	double maxVelTilt;
	double maxAccTilt;

	char buffer[TAMDEFAULTBUFFER];

	mg_printf(conn, "%s", jsonReplyStart);

	ctx = modbus_new_tcp(SERVER, PORT);
//modbus_set_debug(ctx, TRUE);

	if (modbus_connect(ctx) == -1) {
		sprintf(buffer, "{ \"erro\" : \"%s\" }", modbus_strerror(errno));
		mg_write(conn, buffer, strlen(buffer));
		modbus_free(ctx);
		return "1";
	}
	//AZIMUTE
// leitura de velocidade de posicionamento de azimute
	modbus_read_input_registers(ctx, 72, 1, &val.val0);
	VPosAzi = val.val;
	velPosAzimute = val.val / 100;

// leitura de aceleracao de posicionamento de azimute
	modbus_read_input_registers(ctx, 74, 1, &val.val0);
	APosAzi = val.val;
	accPosAzimute = val.val / 100;

// leitura de maxima velocidade de azimute
	modbus_read_input_registers(ctx, 90, 1, &val.val0);
	MaxVelAzi = val.val;
	maxVelAzimute = val.val / 100;

// leitura de maxima aceleracao de azimute
	modbus_read_input_registers(ctx, 92, 1, &val.val0);
	MaxAccAzi = val.val;
	maxAccAzimute = val.val / 100;

	//TILT
// leitura de velocidade de posicionamento de tilt
	modbus_read_input_registers(ctx, 62, 1, &val.val0);
	VPosTilt = val.val;
	velPosTilt = val.val / 100;

// leitura de aceleracao de posicionamento de tilt
	modbus_read_input_registers(ctx, 64, 1, &val.val0);
	APosTilt = val.val;
	accPosTilt = val.val / 100;

// leitura de maxima velocidade de tilt
	modbus_read_input_registers(ctx, 80, 1, &val.val0);
	MaxVelTilt = val.val;
	maxVelTilt = val.val / 100;

// leitura de maxima aceleracao de tilt
	modbus_read_input_registers(ctx, 82, 1, &val.val0);
	MaxAccTilt = val.val;
	maxAccTilt = val.val / 100;

	sprintf(buffer, "{ "
			"\"VelPosAzimuteLida\":\"%.3f\","
			"\"AccPosAzimuteLida\":\"%.2f\","
			"\"maxVelAzimuteLida\":\"%.3f\","
			"\"maxAccAzimuteLida\":\"%.2f\","
			"\"VelPosTiltLida\":\"%.3f\","
			"\"AccPosTiltLida\":\"%.2f\","
			"\"maxVelTiltLida\":\"%.3f\","
			"\"maxAccTiltLida\":\"%.2f\""
			"}", velPosAzimute, accPosAzimute, maxVelAzimute, maxAccAzimute,
			velPosTilt, accPosTilt, maxVelTilt, maxAccTilt);

// fprintf(stdout, "%s", buffer);

	mg_write(conn, buffer, strlen(buffer));

	fprintf(stdout, "getConfig %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n",
			velPosAzimute, accPosAzimute, maxVelAzimute, maxAccAzimute,
			velPosTilt, accPosTilt, maxVelTilt, maxAccTilt);

	modbus_close(ctx);
	modbus_free(ctx);

	return "1";

}

char *executaPosicaoAziScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char buffer[tamData];
	strcpy(buffer, "\"ok\"");

	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));
	return "1";

}

char *executaVelocidadeAziScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {
	char buffer[tamData];
	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));
	return "1";

}

char *executaPosicaoTiltScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {
	char buffer[tamData];
	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));
	return "1";

}

char *executaVelocidadeTiltScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {
	char buffer[tamData];
	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));
	return "1";

}

char *executaTempoScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char buffer[tamData];
	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));
	return "1";
}

char *executaLimpaScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {
	char buffer[tamData];
	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));
	return "1";
}

char *executaScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	modbus_t *ctx;

	stop = 0;

	int cont;

	double tempo;
	double accAzi, accTilt, posAzi, velAzi, posTilt, velTilt;

	char hiddentxt[tamData];
	char buffer[tamData];
	char strpos[6];
	char strvel[6];
	char stracc[6];
	char strtempo[4];

	mg_printf(conn, "%s", jsonReplyStart);

	ctx = modbus_new_tcp(SERVER, PORT);
	//modbus_set_debug(ctx, TRUE);

	if (modbus_connect(ctx) == -1) {
		sprintf(buffer, "{ \"erro\" : \"%s\" }", modbus_strerror(errno));
		mg_write(conn, buffer, strlen(buffer));
		modbus_free(ctx);
		return "1";
	}

	mg_get_var(data, tamData, "hiddentxt", hiddentxt, sizeof(hiddentxt) - 1);

	char * ptr = hiddentxt;
	system("clear");
	tempo = 0.0;

	ptr = hiddentxt;
	while (*ptr != 'z' && stop == 0) {
		switch (*ptr) {
		case 'a':
			cont = 0;
			do {
				ptr++;
				strpos[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			posAzi = atof(strpos);
			executaPosicionamentoAzimute(posAzi);
			break;
		case 'b':
			cont = 0;
			do {
				ptr++;
				strvel[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			velAzi = atof(strvel);
			executaVelocidadeAzimute(velAzi);
			break;
		case 'c':
			cont = 0;
			do {
				ptr++;
				strpos[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			posTilt = atof(strpos);
			executaPosicionamenteTilt(posTilt);
			break;
		case 'd':
			cont = 0;
			do {
				ptr++;
				strvel[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			velTilt = atof(strvel);
			executaVelocidadeTilt(velTilt);
			break;
		case 'e':
			cont = 0;
			do {
				ptr++;
				strtempo[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			tempo = atof(strtempo);
			sleep(tempo); // 1 s do sleep(1) da função executaPoscionamento... ou executaVelocidade...
			break;
		case 'f':
			cont = 0;
			do {
				ptr++;
				stracc[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			accAzi = atof(stracc) * 100.0;
			double maxacelazi = leInputRegs(82);
			if (accAzi > maxacelazi)
				accazi = maxacelazi;
			escreveregs(accAzi, 74);
			escreveregs(accAzi, 76);
			escreveregs(accAzi, 50);
			escreveregs(accAzi, 52);
			break;
		case 'g':
			cont = 0;
			do {
				ptr++;
				strvel[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			double velpazi = atof(strvel) * 100.0;
			double maxvelazi = leInputRegs(90);
			if (velpazi > maxvelazi)
				velpazi = maxvelazi;
			escreveregs(velpazi, 72);
			escrevereg(2, 20);
			sleep(1);
			tempo += 1;
			break;
		case 'h':
			cont = 0;
			do {
				ptr++;
				stracc[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			accTilt = atof(stracc) * 100.0;
			double maxaceltilt = leInputRegs(92);
			if (accTilt > maxaceltilt)
				accTilt = maxaceltilt;
			escreveregs(accTilt, 64);
			escreveregs(accTilt, 66);
			escreveregs(accTilt, 40);
			escreveregs(accTilt, 42);
			break;
		case 'i':
			cont = 0;
			do {
				ptr++;
				strvel[cont] = *ptr;
				cont++;
			} while (*ptr != '\n');
			double velptilt = atof(strvel) * 100.0;
			double maxveltilt = leInputRegs(80);
			if (velptilt > maxveltilt)
				velptilt = maxveltilt;
			escreveregs(velptilt, 62);
			escrevereg(2, 20);
			sleep(1);
			break;
		default:
			break;
		}
		ptr++;
	}

	fprintf(stdout, "%s\n", "Script finished");

	return "1";
}

char *setAccAzi(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char buffer[tamData];
	char acelaziscript[tamData];

	/*


	 mg_get_var(data, tamData, "acelaziscript", acelaziscript,
	 sizeof(acelaziscript) - 1);

	 accazi = atof(acelaziscript) * 100.0;
	 double maxacelazi = leInputRegs(82);
	 if (accazi > maxacelazi) accazi = maxacelazi;

	 escreveregs(accazi, 74);
	 escreveregs(accazi, 76);
	 escreveregs(accazi, 50);
	 escreveregs(accazi, 52);

	 //fprintf(stdout, "Acc Azi->%.2f\n", accazi);

	 */

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *setVelPosAzi(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char buffer[tamData];
	char velposicaziscript[tamData];

	/*

	 mg_get_var(data, tamData, "velposicaziscript", velposicaziscript,
	 sizeof(velposicaziscript) - 1);

	 velposazi = atof(velposicaziscript) * 100.0;
	 double maxvelazi = leInputRegs(90);
	 if (velposazi > maxvelazi) velposazi = maxvelazi;
	 escreveregs(velposazi, 72);
	 escrevereg(2, 20);
	 sleep(1);

	 //fprintf(stdout, "Vel Pos Azi->%.2f\n", velposazi / 100);

	 */

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *setAccTilt(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char buffer[tamData];
	char aceltiltscript[tamData];

	/*


	 mg_get_var(data, tamData, "aceltiltscript", aceltiltscript,
	 sizeof(aceltiltscript) - 1);

	 acctilt = atof(aceltiltscript) * 100.0;
	 double maxaceltilt = leInputRegs(92);
	 if (acctilt > maxaceltilt) acctilt = maxaceltilt;

	 //fprintf(stdout, "Acc Tilt->%.2f\n", acctilt / 100);

	 escreveregs(acctilt, 64);
	 escreveregs(acctilt, 66);
	 escreveregs(acctilt, 40);
	 escreveregs(acctilt, 42);


	 */

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

char *setVelPosTilt(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char buffer[tamData];
	char velposictiltscript[tamData];

	/*

	 mg_get_var(data, tamData, "velposictiltscript", velposictiltscript,
	 sizeof(velposictiltscript) - 1);

	 velpostilt = atof(velposictiltscript) * 100.0;
	 double maxveltilt = leInputRegs(80);
	 if (velpostilt > maxveltilt) velpostilt = maxveltilt;
	 escreveregs(velpostilt, 62);
	 escrevereg(2, 20);
	 sleep(1);

	 //fprintf(stdout, "Vel Pos Tilt->%.2f\n", velpostilt / 100);


	 */

	strcpy(buffer, "\"ok\"");
	mg_printf(conn, "%s", jsonReplyStart);
	mg_write(conn, buffer, strlen(buffer));

	return "1";
}

void executaPosicionamentoAzimute(float azimute) {
	float pAzimute = azimute * 1000;

	if (pAzimute < 0) {
		pAzimute = 360000 - abs(pAzimute);
	}

	// Azimuth
	escrevereg(2, 24);
	escrevereg(0, 28);

	escreveregs(pAzimute, 70);
	sleep(1);
	escrevereg(0, 28);
	//sleep(1);
	escrevereg(4, 28);
}

void executaPosicionamenteTilt(float tilt) {
	float pTilt = tilt * 1000;

	if (pTilt < 0) {
		pTilt = 360000 - abs(pTilt);
	}

	escrevereg(2, 22);
	escrevereg(0, 26);

	escreveregs(pTilt, 60);
	sleep(1);
	escrevereg(0, 26);
	//sleep(1);
	escrevereg(4, 26);
}

void executaVelocidadeAzimute(float vel) {

	float velocidade = vel * 100.0;
	if (abs(velocidade) > 50000.0) {
		if (velocidade < 0.0) {
			velocidade = -50000.0;
		} else {
			velocidade = 50000.0;
		}
	}

	escrevereg(1, 22);
	escrevereg(1, 24);

	escrevereg(28, 0);

	if (velocidade < 0.0) {
		escreveregs(abs(velocidade), 56);
		escrevereg(2, 28);      //reverso
	} else if (velocidade > 0.0) {
		escreveregs(velocidade, 56);
		escrevereg(1, 28);      //avante
	} else {
		escrevereg(0, 28);
	}
}

void executaVelocidadeTilt(float vel) {

	float velocidade = vel * 100.0;
	if (abs(velocidade) > 20000.0) {
		if (velocidade < 0.0) {
			velocidade = -20000.0;
		} else {
			velocidade = 20000.0;
		}
	}

	escrevereg(1, 22);
	escrevereg(1, 24);

	escrevereg(26, 0);

	if (velocidade < 0.0) {
		escreveregs(abs(velocidade), 46);
		escrevereg(2, 26);      //reverso
	} else if (velocidade > 0.0) {
		escreveregs(velocidade, 46);
		escrevereg(1, 26);     //avante
	} else {
		escrevereg(0, 26);
	}
}

char *executaGravacao(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData) {

	char timeout[tamData];
	mg_get_var(data, tamData, "timeout", timeout, sizeof(timeout) - 1);
	tmout = atoi(timeout);

	fp = fopen("\\Windows\\Temp\\dadosMesa.txt", "w+");
	if(fp == NULL) {
		fprintf(stdout, "Erro na abertura. Detalhes --> %s\n", errno);
		return "1";
	}
	fprintf(fp, "Tempo entre leituras = %ldms\n\n", tmout);
	counter = 1;
	rec = 1;
	start = clock();
	return "1";
}

double leInputRegs(int endereco) {

	modbus_t *ctx;
	uint16_t dado;

	ctx = modbus_new_tcp(SERVER, PORT);
	///modbus_set_debug(ctx, TRUE);

	if (modbus_connect(ctx) == -1) {
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return -1;
	}

	modbus_read_input_registers(ctx, endereco, 1, &dado);
	/* Close the connection */
	modbus_close(ctx);
	modbus_free(ctx);

	return (double) dado;
}

double * leDados() {
	modbus_t *ctx;
	dWord val;
	double azimutePosicaoAtual;
	double azimuteVelocidadeAtual;
	double tiltPosicaoAtual;
	double tiltVelocidadeAtual;
	double temp;

	static double ret[4];

	char *buffer[TAMBUFFERPARAMETROS];

	ctx = modbus_new_tcp(SERVER, PORT);
	//modbus_set_debug(ctx, TRUE);

	if (modbus_connect(ctx) == -1) {
		fprintf(stdout, "%s\n", "Connection error");
		modbus_free(ctx);
		return 0;
	}

	// leitura de posicao atual de azimute
	modbus_read_input_registers(ctx, 0, 1, &val.val0);
	modbus_read_input_registers(ctx, 1, 1, &val.val1);
	temp = val.val;
	//azimutePosicaoAtual = (temp < 0.0 ? (temp + 360.0) : temp) / 1000000.0;
	azimutePosicaoAtual = temp;
	ret[0] = azimutePosicaoAtual / 1000000.0;
	;

	// leitura de velocidade atual de azimute MÉDIA DE 500 LEITURAS
	modbus_read_input_registers(ctx, 2, 1, &val.val0);
	modbus_read_input_registers(ctx, 3, 1, &val.val1);
	azimuteVelocidadeAtual = val.valsigned / 1000000.0;
	ret[1] = azimuteVelocidadeAtual;

	// leitura de posicao atual de tilt
	modbus_read_input_registers(ctx, 4, 1, &val.val0);
	modbus_read_input_registers(ctx, 5, 1, &val.val1);
	temp = val.val;
	// tiltPosicaoAtual = (temp < 0.0 ? (temp + 360.0) : temp) / 1000000.0;
	tiltPosicaoAtual = val.val / 1000000.0;
	ret[2] = tiltPosicaoAtual;

	// leitura de velocidade atual de tilt MÉDIA DE 500 LEITURAS
	modbus_read_input_registers(ctx, 6, 1, &val.val0);
	modbus_read_input_registers(ctx, 7, 1, &val.val1);
	tiltVelocidadeAtual = val.valsigned / 1000000.0;
	ret[3] = tiltVelocidadeAtual;

	modbus_close(ctx);
	modbus_free(ctx);

	return ret;
}
