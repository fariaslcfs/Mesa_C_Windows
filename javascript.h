#ifndef __MESAMONGOOSE__MESA__
#define __MESAMONGOOSE__MESA__

#include "mongoose.h"

char *mesaMongooseJS(struct mg_connection *conn,
		  const struct mg_request_info *request,
		  const char *data, int tamData);

#endif
