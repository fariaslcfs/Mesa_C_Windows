CC = gcc
GCC_WARNS   = -W -Wall -pedantic
# ORIGINAL INFAX CFLAGS = -std=c++11 -ggdb -O0 -DNDEBUG -DNO_CGI -DNO_SSL -lmodbus -I/usr/include/modbus
CFLAGS = -std=c++11 $(GCC_WARNS) -ggdb -O3 -DNDEBUG -DNO_SSL -lws2_32 -lmodbus
#CFLAGS = -O3 -DNDEBUG -DNO_CGI -DNO_SSL -lmodbus -I /usr/include/modbus
DEPS = mesamongoose.h javascript.h util.h despachante.h mongoose.h mesa.h
OBJS = main.o javascript.o util.o despachante.o mongoose.o mesa.o

all: mesamongoose 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

mesamongoose: $(OBJS) 
	gcc -o $@ $(OBJS) $(CFLAGS) -lpthread

clean:
	rm -f mesamongoose *.o *~ *.aux *.log *.tex *.dvi book.pdf *.exe 