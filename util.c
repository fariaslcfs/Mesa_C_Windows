#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*#include <openssl/sha.h>*/

#include "util.h"

/**
* \file util.c
* \brief arquivo onde esta as funcoes uteis para a aplicacao
*  Arquivo com funcoes uteis para o sistema como copiar,
   codificar e pegar o buffer dos parametros  
*/

const char *jsonReplyStart =
  "HTTP/1.1 200 OK\r\n"
  "Cache: no-cache\r\n"
  "Content-Type: text/json\r\n"
  "\r\n";

const char *javascriptReplyStart =
  "HTTP/1.1 200 OK\r\n"
  "Cache: no-cache\r\n"
  "Content-Type: application/x-javascript\r\n"
  "\r\n";

const char *txtReplyStart =
  "HTTP/1.1 200 OK\r\n"
  "Cache: no-cache\r\n"
  "Content-Type: text/plain\r\n"
  "\r\n";


/**@fn meustrcpy( char *target, const char source, size_t len)
   @brief faz copia do codigo para o target
   Funcao para copiar codigo para o target pasando o caracter '\0'
   para indicar final de linha 
   @param target 
   @param source codigo
   @param len tamanho do source
   @return char
*/

char *meustrcpy(char *target, const char *source, size_t len){
  strncpy(target, source, len);
  target[len - 1] = '\0';
  return target;
}

/**@fn codifica(char *fonte, int tamFonte, char *destino, int tamDestino)
   @brief codifica a fonte
   Funcao para codificar a fonte copiando para o destino
   @param fonte fonte a ser copiada
   @param tamFonte tamanho da fonte
   @param destino destinho a se copiado
   @param tamDestino tamanho do destino
   @return int 
*/

int codifica(char *fonte, int tamFonte,
             char *destino, int tamDestino) {
  strncpy(destino, fonte, tamDestino);
} 

/**@fn sha256(char *string, char outputBuffer[65] )
   @brief
   @param string string passada
   @param outputBuffer buffer de saida
   @return
*/

/*
void sha256(char *string, char outputBuffer[65]) {
  unsigned char hash[SHA256_DIGEST_LENGTH];
  SHA256_CTX sha256;
  SHA256_Init(&sha256);
  SHA256_Update(&sha256, string, strlen(string));
  SHA256_Final(hash, &sha256);
  int i = 0;
  for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
      sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
    }
  outputBuffer[64] = 0;
}   
*/

/**@fn getBufferParametros( struct mg_connection *conn, const struct mg_request_inf *ri
                            char *dst, size_t dst_len )
   @brief pega os buffer dos parametros
   Funcao para pegar parametros da conexao 
   @param conn conexao
   @param ri metodo de requisicao
   @param dst
   @param dst_len tamanho do dst
   @return
*/

void getBufferParametros(struct mg_connection *conn,
				const struct mg_request_info *ri,
				char *dst, size_t dst_len) {
  char *var, *buf;
  size_t buf_len;
  const char *cl;
  int var_len;

  buf_len = 0;
  var = buf = NULL;
  cl = mg_get_header(conn, "Content-Length");
  if ((!strcmp(ri->request_method, "POST") ||
       !strcmp(ri->request_method, "PUT"))
      && cl != NULL) {
    buf_len = atoi(cl);
    buf = malloc(buf_len + 1);
    /* Read in two pieces, to test continuation */
    if (buf_len > 2) {
      mg_read(conn, buf, 2);
      mg_read(conn, buf + 2, buf_len - 2);
    } else {
      mg_read(conn, buf, buf_len);
    }
    buf[buf_len] = '\x0';
  } else if (ri->query_string != NULL) {
    buf_len = strlen(ri->query_string);
    buf = malloc(buf_len + 1);
    strcpy(buf, ri->query_string);
  }
  if(buf != NULL) {
    strncpy(dst, buf, dst_len);
    free(buf);
  }
  else 
    dst[0] = '\x0';
}

void printVerbose(char *s) {
   if (verbose) {
      fprintf(stdout, "%s", s);
	  fflush(stdout);
   }
}

