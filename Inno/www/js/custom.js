var azimutePosicao = {
	val : 0,
	idx : 0,
	serie : []
};

var azimuteVelocidade = {
	val : 0,
	idx : 0,
	serie : []
};

var tiltPosicao = {
	val : 0,
	idx : 0,
	serie : []
};

var tiltVelocidade = {
	val : 0,
	idx : 0,
	serie : []
};

var shiftSerie = function(serie, janela) {
	for (i = 1; i < janela; i++) {
		serie[i - 1] = serie[i];
	}
}

var atualizaSerie = function(medida, tamJanela) {
	if (medida.idx < tamJanela) {
		medida.serie[medida.idx] = [ medida.idx, medida.val ];
		medida.idx++;
	} else {
		shiftSerie(medida.serie, tamJanela);
		medida.serie[tamJanela - 1] = [ medida.idx, medida.val ];
		medida.idx++;
	}
}

var plotGraficoVel = true;

var plotGraficoPos = true;

var atualizaGrafico = function() {
	var tamJanela = 40;
	atualizaSerie(azimutePosicao, tamJanela);
	atualizaSerie(azimuteVelocidade, tamJanela);
	atualizaSerie(tiltPosicao, tamJanela);
	atualizaSerie(tiltVelocidade, tamJanela);
	if (plotGraficoPos) {
		var plot = $.plot($("#stats-azimute"), [ {
			data : azimutePosicao.serie,
			label : "Azimute",
			lines : {
				show : true,
				fill : false,
				lineWidth : 2
			},
			shadowSize : 0
		}, {
			data : tiltPosicao.serie,
			label : "Tilt",
			lines : {
				show : true,
				fill : false
			}
		} ], {
			grid : {
				hoverable : true,
				clickable : true,
				tickColor : "rgba(255,255,255,0.05)",
				borderWidth : 0
			},
			legend : {
				show : true
			},
			colors : [ "rgba(255,255,255,0.8)", "rgba(255,255,0,0.6)",
					"rgba(255,255,255,0.4)", "rgba(255,255,255,0.2)" ],
			xaxis : {
				show : false,
				color : "rgba(255,255,255,0.8)"
			},
			yaxis : {
				ticks : 5,
				tickDecimals : 0,
				color : "rgba(255,255,255,0.8)"
			},
		});
	}

	if (plotGraficoVel) {
		var plot = $.plot($("#stats-tilt"), [ {
			data : azimuteVelocidade.serie,
			label : "Azimute",
			lines : {
				show : true,
				fill : false,
				lineWidth : 2
			},
			shadowSize : 0
		}, {
			data : tiltVelocidade.serie,
			label : "Tilt",
			lines : {
				show : true,
				fill : true
			}
		} ], {
			grid : {
				hoverable : true,
				clickable : true,
				tickColor : "rgba(255,255,255,0.05)",
				borderWidth : 0
			},
			legend : {
				show : true
			},
			colors : [ "rgba(255,255,255,0.8)", "rgba(255,255,0,0.6)",
					"rgba(255,255,255,0.4)", "rgba(255,255,255,0.2)" ],
			xaxis : {
				show : false,
				color : "rgba(255,255,255,0.8)"
			},
			yaxis : {
				ticks : 5,
				tickDecimals : 0,
				color : "rgba(255,255,255,0.8)"
			},
		});
	}
}

var atualizaMostradores = function() {
	$("#azimutePosicaoLida").html(azimutePosicao.val);
	$("#azimuteVelocidadeLida").html(azimuteVelocidade.val);
	$("#tiltPosicaoLida").html(tiltPosicao.val);
	$("#tiltVelocidadeLida").html(tiltVelocidade.val);
};

var cont = 0;
var accVAzi = 0; 
var accVTilt = 0;
var lim = 1;
var timeout = 100;
var atualizaDados = function() {
	if(document.location.href.indexOf('index.html') > -1){
		timeout = $('#timeout').val();
		lim = $('#limite').val();
	}
	else{
		timeout = localStorage.getItem('timeoutStore');
		lim = localStorage.getItem('limiteStore');
	}

	$.ajax({
		url : "/api/dados.json",
		cache : false
	}).done(function(dados) {
		if(lim > 1){
			accVAzi = accVAzi + parseFloat(dados.azimuteVelocidadeLida);
			accVTilt = accVTilt + parseFloat(dados.tiltVelocidadeLida);
			if(cont >= lim){
				azimuteVelocidade.val = (accVAzi / lim).toFixed(2);
				tiltVelocidade.val = (accVTilt / lim).toFixed(2);
				tiltPosicao.val = dados.tiltPosicaoLida;
				tiltVelocidade.val = dados.tiltVelocidadeLida;
				cont = 0; accVAzi = 0; accVTilt = 0;
			}
		}
		else{
			azimutePosicao.val = dados.azimutePosicaoLida;
			azimuteVelocidade.val = dados.azimuteVelocidadeLida;
			tiltPosicao.val = dados.tiltPosicaoLida;
			tiltVelocidade.val = dados.tiltVelocidadeLida;
		}
		cont++;
		atualizaMostradores();
		atualizaGrafico();
		setTimeout(atualizaDados, timeout);
	});
};

var setConfig = function() {
	var VelPosAzimute = $("#VelPosAzimute").val()
	var AccPosAzimute = $("#AccPosAzimute").val()
	var maxVelAzimute = $("#maxVelAzimute").val()
	var maxAccAzimute = $("#maxAccAzimute").val()
	var VelPosTilt = $("#VelPosTilt").val()
	var AccPosTilt = $("#AccPosTilt").val()
	var maxVelTilt = $("#maxVelTilt").val()
	var maxAccTilt = $("#maxAccTilt").val()
	if (isNaN(VelPosAzimute) || VelPosAzimute.length == 0 || VelPosAzimute[VelPosAzimute.length - 1] == '.'
			|| isNaN(AccPosAzimute) || AccPosAzimute.length == 0 || AccPosAzimute[AccPosAzimute.length - 1] == '.'
			|| isNaN(maxVelAzimute) || maxVelAzimute.length == 0 || maxVelAzimute[maxVelAzimute.length - 1] == '.'
			|| isNaN(maxAccAzimute) || maxAccAzimute.length == 0 || maxAccAzimute[maxAccAzimute.length - 1] == '.'
			|| isNaN(VelPosTilt) || VelPosTilt.length == 0 || VelPosTilt[VelPosTilt.length - 1] == '.'
			|| isNaN(AccPosTilt) || AccPosTilt.length == 0 || AccPosTilt[AccPosTilt.length - 1] == '.'
			|| isNaN(maxVelTilt) || maxVelTilt.length == 0 || maxVelTilt[maxVelTilt.length - 1] == '.'
			|| isNaN(maxAccTilt) || maxAccTilt.length == 0 || maxAccTilt[maxAccTilt.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return
	}
	$.ajax({
		url : "/api/setConfig.json",
		cache : false,
		data : {
			"VelPosAzimute" : VelPosAzimute,
			"AccPosAzimute" : AccPosAzimute,
			"maxVelAzimute" : maxVelAzimute,
			"maxAccAzimute" : maxAccAzimute,
			"VelPosTilt" : VelPosTilt,
			"AccPosTilt" : AccPosTilt,
			"maxVelTilt" : maxVelTilt,
			"maxAccTilt" : maxAccTilt,
		}
	}).done(function() {
		alert("Configuração salva nos registros da mesa.");
	});
};

var getConfig = function() {
	$.ajax({
		url : "/api/getConfig.json",
		cache : false
	}).done(function(dados) {
		$("#VelPosAzimute").val(dados.VelPosAzimuteLida)
		$("#AccPosAzimute").val(dados.AccPosAzimuteLida)
		$("#maxVelAzimute").val(dados.maxVelAzimuteLida)
		$("#maxAccAzimute").val(dados.maxAccAzimuteLida)
		$("#VelPosTilt").val(dados.VelPosTiltLida)
		$("#AccPosTilt").val(dados.AccPosTiltLida)
		$("#maxVelTilt").val(dados.maxVelTiltLida)
		$("#maxAccTilt").val(dados.maxAccTiltLida)
		// alert("Conf Lida");
	});
};

var executaPosicionamento = function() {
	var posAzimute = $("#posAzimute").val()
	var posTilt = $("#posTilt").val()
	if (isNaN(posAzimute) || posAzimute.length == 0 || posAzimute[posAzimute.length - 1] == '.'
			|| isNaN(posTilt) || posTilt.length == 0 || posTilt[posTilt.length - 1] == '.') {
		alert("Entre com valores numéricos [0.0-360.0]")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/executaPosicionamento.json",
		data : {
			"posAzimute" : posAzimute,
			"posTilt" : posTilt
		},
		cache : false
	}).done(function() {
		// alert("pos");
	});
};

var executaJog = function() {
	var velAzimute = $("#velAzimute").val()
	var velTilt = $("#velTilt").val()
	if (isNaN(velAzimute) || velAzimute.length == 0 || velAzimute[velAzimute.length - 1] == '.' 
			|| isNaN(velTilt) || velTilt.length == 0 || velTilt[velTilt.length - 1] == '.') {
		alert("Entre com valores numéricos [0-200 para Tilt e 0-500 para Azimute")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/executaJog.json",
		data : {
			"velAzimute" : velAzimute,
			"velTilt" : velTilt
		},
		cache : false
	}).done(function() {
		// alert("jog");
	});
};

var executaJogAutomatizada = function() {
	var inctime = $("#inctime").val()
	var incveltilt = $("#incveltilt").val()
	var incvelazi = $("#incvelazi").val()
	var velinitilt = $("#velinitilt").val()
	var velendtilt = $("#velendtilt").val()
	var veliniazi = $("#veliniazi").val()
	var velendazi = $("#velendazi").val()
	if (isNaN(inctime) || inctime.length == 0 || inctime[inctime.length - 1] == '.'
			|| isNaN(incveltilt) || incveltilt.length == 0 || incveltilt[incveltilt.length - 1] == '.'
			|| isNaN(incvelazi) || incvelazi.length == 0 || incvelazi[incvelazi.length - 1] == '.'
			|| isNaN(velinitilt) || velinitilt.length == 0 || velinitilt[velinitilt.length - 1] == '.'
			|| isNaN(velendtilt) || velendtilt.length == 0 || velendtilt[velendtilt.length - 1] == '.' 
			|| isNaN(veliniazi) || veliniazi.length == 0 || veliniazi[veliniazi.length - 1] == '.'
			|| isNaN(velendazi) || velendazi.length == 0 || velendazi[velendazi.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/executaJogAutomatizada.json",
		data : {
			"inctime" : inctime,
			"incveltilt" : incveltilt,
			"incvelazi" : incvelazi,
			"velinitilt" : velinitilt,
			"velendtilt" : velendtilt,
			"veliniazi" : veliniazi,
			"velendazi" : velendazi,
		},
		cache : false
	}).done(function() {
		// alert("jog");
	});
};

var executaHabJog = function() {
	var velPosAziHab = $('#velPosAziHab').val()
	var velPosTiltHab = $('#velPosTiltHab').val()
	if (isNaN(velPosAziHab) || velPosAziHab.length == 0 || velPosAziHab[velPosAziHab.length - 1] == '.' 
			|| velPosAziHab[0] == '-' || isNaN(velPosTiltHab) || velPosTiltHab.length == 0 || velPosTiltHab[velPosTiltHab.length - 1] == '.'
			|| velPosTiltHab[0] == '-' || Math.abs(velPosAziHab) > 500.0 || Math.abs(velPosTiltHab) > 200.0) {
		alert("Entre com valores numéricos e positivos. Máximo de 200°/s para Tilt e de 500°/s para Azimute")
		location.reload(true)
		return
	}
	
	if(parseFloat(velPosAziHab) >= 100.0 || parseFloat(velPosTiltHab) >= 100.0){
		var r = confirm("Velocidade alta! Tem certeza?\n\n");
		if (r == false) {
			document.getElementById('checkHabJog').checked = false;
			return;
		}
	}
	alert('Verifique se as chaves estão na posicão neutra (central).\nSe alguma chave estiver em posição de acionamento,\no eixo correspondente será acionado imediatamente.')
 	
	$.ajax({
		url : "/api/executaHabJog.json",
		data : {
			"velPosAziHab" : velPosAziHab,
			"velPosTiltHab" : velPosTiltHab
		},
		cache : false
	}).done(function() {
		// alert("Modo Jog habilitado");
	});
};

var executaDesJog = function() {
	$.ajax({
		url : "/api/executaDesJog.json",
		cache : false
	}).done(function() {
		// alert("Modo Jog Desabilitado");
	});
};

var executaParar = function() {
	$.ajax({
		url : "/api/executaParar.json",
		cache : false
	}).done(function() {
		alert("Velocidades Zeradas");
		location.reload(true);
	});
};

var executaPararGravacao = function() {
	var reccircle = document.getElementById("reccircle");
	reccircle.setAttributeNS(null, 'style', 'fill: black;');
	$.ajax({
		url : "/api/executaPararGravacao.json",
		cache : false
	}).done(function() {
		alert("Dados gravados em \\Windows\\Temp\\dadosMesa.txt");
		location.reload();
	});
};

var executaReset = function() {
	$.ajax({
		url : "/api/executaReset.json",
		cache : false
	}).done(function() {
		alert("Mesa pronta pra uso.");
	});
};

var executaEmergencia = function() {
	$.ajax({
		url : "/api/emergencia.json",
		cache : false
	}).done(function() {
		alert("Emergência. Relé de segurança acionado.	");
	});
};

var executaPosicaoAziScript = function() {
	var pos = $("#posaziscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	if (isNaN(pos) || pos.length == 0 || pos[pos.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	if (pos < -360 || pos > 360) {
		alert("Entre com valores entre 0 e +-360°")
		location.reload(true)
		return
	}
	$.ajax({
		url : "/api/executaPosicaoAziScript.json",
		cache : false
	}).done(function() {
		txt.value += "Azimute em " + pos.trim() + " °\n";
		// location.reload(false);
		// txt.scrollTop = txt.scrollHeight;
		// alert("Inserido " + pos + " ° no script");
		htxt.value += 'a' + pos + '\n';
		// alert(htxt.value);
	});
};

var executaVelocidadeAziScript = function() {
	var vel = $("#velaziscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	if (isNaN(vel) || vel.length == 0 || vel[vel.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	if (vel > 500 || vel < -500) {
		alert("Entre com valores entre 0 em +-500°/s")
		location.reload(true)
		return
	}
	$.ajax({
		url : "/api/executaVelocidadeAziScript.json",
		cache : false
	}).done(function() {
		txt.value += "velocidade Azimute = " + vel.trim() + " °/s\n";
		// location.reload(false);
		// txt.scrollTop = txt.scrollHeight;
		// alert("Inserida " + vel + " °/s no Script");
		htxt.value += 'b' + vel + '\n';
		// alert(htxt.value);
	});
};

var executaPosicaoTiltScript = function() {
	var pos = $("#postiltscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	if (isNaN(pos) || pos.length == 0 || pos[pos.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	if (pos < -360 || pos > 360) {
		alert("Entre com valores entre 0 e +-360°")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/executaPosicaoTiltScript.json",
		cache : false
	}).done(function() {
		txt.value += "Tilt em " + pos.trim() + " °\n";
		// location.reload(false);
		// txt.scrollTop = txt.scrollHeight;
		// alert("Inserido " + pos + " ° no script");
		htxt.value += 'c' + pos + '\n';
		// alert(htxt.value);
	});
};

var executaVelocidadeTiltScript = function() {
	var vel = $("#veltiltscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	if (isNaN(vel) || vel.length == 0 || vel[vel.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	if (vel > 200 || vel < -200) {
		alert("Entre com valores entre 0 em +-200°/s")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/executaVelocidadeTiltScript.json",
		cache : false
	}).done(function() {
		txt.value += "Velocidade Tilt = " + vel.trim() + " °/s\n";
		// location.reload(false);
		// txt.scrollTop = txt.scrollHeight;
		// alert("Inserida " + vel + " °/s no Script");
		htxt.value += 'd' + vel + '\n';
		// alert(htxt.value);
	});
};

var executaTempoScript = function() {
	var tempo = $("#temposcript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	if (isNaN(tempo) || tempo.length == 0 || tempo[tempo.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	if (tempo > 7200) {
		alert("Entre com valores entre 0 e 7200s")
		location.reload(true)
		return
	}
	$.ajax({
		url : "/api/executaTempoScript.json",
		cache : false
	}).done(function() {
		txt.value += "Permanece " + tempo.trim() + " s\n";
		// location.reload(false);
		// txt.scrollTop = txt.scrollHeight;
		// alert("Inserido " + tempo + " s no script");
		htxt.value += 'e' + tempo + '\n';
		// alert(htxt.value);
	});
};

var executaLimpaScript = function() {
	$.ajax({
		url : "/api/executaLimpaScript.json",
		cache : false
	}).done(function() {
		document.getElementById("txtcomandos").value = '';
		document.getElementById("hiddentxt").value = '';
		// alert("Script limpo");
		location.reload(true);
	});
};

var toHHMMSS = (secs) => {
    var sec_num = parseInt(secs, 10)    
    var hours   = Math.floor(sec_num / 3600) % 24
    var minutes = Math.floor(sec_num / 60) % 60
    var seconds = sec_num % 60    
    return [hours,minutes,seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v,i) => v !== "00" || i > 0)
        .join(":")
}

var leTempoScript = function() {
	var tempo;
	var tempoFormatado = new Date(null);
	var hiddentxt = document.getElementById("hiddentxt").value + 'z';
	var tempotxt = document.getElementById("tempo");
	var mat = [];
	tempo = 0;
	for(var i = 0; i < hiddentxt.length; i++){
		if(hiddentxt[i] == 'e'){
			var c = 0;
			for(var j = i + 1; hiddentxt[j] != '\n'; j++){
				mat[c] = hiddentxt[j];
				c++;
			}
			tempo += parseFloat(mat);
		}
	}
	tempo += 4; // 2s do exec.posic e 2s do ajuste de velocidade
	t = setInterval(myTimer, 1000);
	function myTimer() {
		tempotxt.value = "\n\n\n\n" + toHHMMSS(tempo);
		tempo--;
		if (tempo < 0) {
			clearInterval(t);
			tempotxt.value = "\n\n\n\nFim do script";
		}
	};
};

var executaScript = function() {
	var htxt = document.getElementById("hiddentxt").value;
	var tempotxt = document.getElementById("tempo");

	if (htxt.length == 0) {
		alert("É necessário inserir os comandos");
		location.reload(true);
		return;
	}
	
	var mat = [];
	var i = 0;
	var idx = 0;
	var teste = 0;
	var c;

	while(i < htxt.length){
		c = htxt[i];
		if(c != 'a' && c != 'b' && c != 'c' 
			&& c != 'd' && c != 'e' && c != 'f'
			&& c != 'g' && c != 'h' && c != 'i'){
			teste++;
			break;
		}
		else{
			i++;
			c = htxt[i];

			while(c != '\n' && i < htxt.length){
				mat[idx] = c ;
				i++;
				c = htxt[i];
				if(c == '\n') break;
				idx++;
			}

			if(mat[mat.length - 1] == '.'){
				teste++;
				break;
			}

			m = mat.join("");

			if(isNaN(m)){
				teste++;
				break;
			}

			idx = 0 ;
			i++;
			mat = [];
		}
	}
	
	if(teste > 0){
		alert("Protocolo inválido. Siga o formato apresentado abaixo:\n\na[azimute]\nb[velocidade azimute]\nc[tilt]\nd[velocidade tilt]\ne[tempo em s]\nf[aceleração azimute]\ng[velocidade posicionamento azimute]\nh[aceleração tilt]\ni[velocidade posicionamento tilt]\n\nObs.: O separador decimal é o ponto (.).");
		location.reload();
		return;
	}

	var r = confirm("TEM CERTEZA?\n\n");
	if (r == false) {
		location.reload(true);
		return;
	}
	
	leTempoScript();
	
	htxt += 'z';

	$.ajax({
		type : "POST",
		url : "/api/executaScript.json",
		dataType : "json",
		data : {
			"hiddentxt" : htxt
		},
		cache : false
	}).done(function() {
	});
};

var executaGravacao = function() {
	var reccircle = document.getElementById("reccircle");
	reccircle.setAttributeNS(null, 'style', 'fill: red;');
	$.ajax({
		url : "/api/executaGravacao.json",
		data : {"timeout" : timeout},
		cache : false
	}).done(function() {
		// location.reload(true);
	});
}

var setAccAzi = function() {
	var accAzi = $("#acelaziscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	var acelaziscript = $("#acelaziscript").val()
	if (isNaN(accAzi) || accAzi.length == 0 || accAzi[accAzi.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/setAccAzi.json",
		data : {
			"acelaziscript" : acelaziscript,
		},
		cache : false
	}).done(function() {
		txt.value += "Aceleração Azimute " + accAzi.trim() + " °/s²\n";
		htxt.value += 'f' + accAzi + '\n';

	});
};

var setVelPosAzi = function() {
	var velPosAzi = $("#velposicaziscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	var velposicaziscript = $("#velposicaziscript").val()
	if (isNaN(velPosAzi) || velPosAzi.length == 0 || velPosAzi[velPosAzi.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/setVelPosAzi.json",
		data : {
			"velposicaziscript" : velposicaziscript,
		},
		cache : false
	}).done(
			function() {
				txt.value += "Velocidade Posicionamento Azimute "
						+ velPosAzi.trim() + " °/s\n";
				htxt.value += 'g' + velPosAzi + '\n';
			});
};

var setAccTilt = function() {
	var accTilt = $("#aceltiltscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	var aceltiltscript = $("#aceltiltscript").val()
	if (isNaN(accTilt) || accTilt.length == 0 || accTilt[accTilt.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/setAccTilt.json",
		data : {
			"aceltiltscript" : aceltiltscript,
		},
		cache : false
	}).done(function() {
		txt.value += "Aceleração Tilt " + accTilt.trim() + " °/s²\n";
		htxt.value += 'h' + accTilt + '\n';
	});
};

var setVelPosTilt = function() {
	var velPosTilt = $("#velposictiltscript").val()
	var txt = document.getElementById("txtcomandos")
	var htxt = document.getElementById("hiddentxt")
	var velposictiltscript = $("#velposictiltscript").val()
	if (isNaN(velPosTilt) || velPosTilt.length == 0 || velPosTilt[velPosTilt.length - 1] == '.') {
		alert("Entre com valores numéricos")
		location.reload(true)
		return

	}
	$.ajax({
		url : "/api/setVelPosTilt.json",
		data : {
			"velposictiltsc+_ript" : velposictiltscript,
		},
		cache : false
	}).done(
			function() {
				txt.value += "Velocidade Posicionamento Tilt "
						+ velPosTilt.trim() + " °/s\n";
				htxt.value += 'i' + velPosTilt + '\n';
			});
};
