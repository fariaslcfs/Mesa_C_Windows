#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdarg.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <getopt.h>
#include "mesamongoose.h"
#include "util.h"
#include "despachante.h"

#ifdef _WIN32
#include <windows.h>
#define sleep(n) Sleep(1000 * n)
#endif

/**
 *\file main.c
 *\brief arquivo inicia da aplicacao
 * Main e responsavel pela inicializacao da aplicacao
 * onde e estartado o Mongoose e configuracao da webapp
 * porta usada(default 8081) e numero de threads(default 5)
 *
 */

static int exit_flag;
static struct mg_context *ctx;
static char* program_name;
int verbose = 1;
int sessiondebug = 0;

/*const char * appdata = getenv("AppData");
char docRoot[TAMDEFAULTBUFFER] = getenv("AppData");
char listPorts[TAMDEFAULTBUFFER] = "8088";
char enableDirectory[TAMDEFAULTBUFFER] = "yes";
char numThreads[TAMDEFAULTBUFFER] = "5";*/

void print_usage(FILE *stream, int exit_code) {
	fprintf(stream, "Usage: %s options\n", program_name);
	fprintf(stream, "-h --help Display this usage information. \n"
			"-d --document root Directory where this application\n"
			"-l --listening port Ports that will be used\n"
			"-e --enabled directory listing  Enabled directory listing\n"
			"-n --number threads Number for threads\n"
			"-v --verbose print log\n"
			"-s --sessiondebug enable session debug\n"
			"-L --latencia\n");
	exit(exit_code);

}

/** @fn void signal_handler(int sig_num)
 @brief funao que fica aguardando instrucao do usuario
 Funcao que agurada instrucao do usuario como um Ctrl+C
 passando para o mongoose
 @param int sig_num
 @return
 */

static void signal_handler(int sig_num) {
	exit_flag = sig_num;
}

/**fn void startMongoose(int argc, char *argv[])
 @brief funcao para iniciar o servidor
 Funcao que inicializacao o servidor mongoose
 @return

 */

static void startMongoose(const char *options[]) {
	ctx = mg_start(eventHandler, NULL, options);
	if (ctx == NULL) {
		fprintf(stderr, "Erro ao iniciar servidor\n");
		exit(1);
	}
}

/**fn void main
 @brief funcao principal
 Funcao que innicializa o servido(funcao principal)
 @param int argc
 @param char argv[]
 @return int
 */
int main(int argc, char **argv) {
	int next_option;
	int latencia;

	char appdata[100] = { 0 };
	char *envvar = getenv("AppData"); // Para poder instalar no diretorio appdata
	if (envvar != NULL) {			  // do usuário logado
	    strncat(appdata, envvar, sizeof(appdata) - 1);
	    strncat(appdata, "\\www", sizeof(appdata) -1);
	}

	char * docRoot = appdata;
	char listPorts[TAMDEFAULTBUFFER] = "8088";
	char enableDirectory[TAMDEFAULTBUFFER] = "yes";
	char numThreads[TAMDEFAULTBUFFER] = "5";

	const char *options[] = { "document_root", docRoot, "listening_ports",
			listPorts, "enable_directory_listing", enableDirectory,
			"num_threads", numThreads,
			NULL };

	const char *short_options = "hd:l:en:vs:L";
	const struct option long_options[] = { { "help", 0, NULL, 'h' }, {
			"document_root", 1, NULL, 'd' },
			{ "listening_ports", 1, NULL, 'l' }, { "enabled_diretory_listing",
					0, NULL, 'e' }, { "num_threads", 1, NULL, 'n' }, {
					"verbose", 0, NULL, 'v' }, { "sessiondebug", 0, NULL, 's' },
			{ "latencia", 0, NULL, 'L' }, { NULL, 0, NULL, 0 } };

	program_name = argv[0];

	do {

		next_option = getopt_long(argc, argv, short_options, long_options,
				NULL);
		switch (next_option) {
		case 'h':/*Help*/
			print_usage(stdout, 0);
			break;
		case '?':/*opcao invalida*/
			print_usage(stderr, 1);
			break;
		case 'd':/*Caminho onde esta a aplicacao (document_root)*/
			strncpy(docRoot, optarg, TAMDEFAULTBUFFER);
			break;
		case 'l':/*listar portas (listening_ports)*/
			strncpy(listPorts, optarg, TAMDEFAULTBUFFER);
			break;
		case 'e':/*habilitar listagem de diretorio(enabled_directory_listing)*/
			strncpy(enableDirectory, "yes", TAMDEFAULTBUFFER);
			break;
		case 'n':/*numero de threads (num threads)*/
			strncpy(numThreads, optarg, TAMDEFAULTBUFFER);
			break;
		case 'v':/*imprime o(s) log(s)*/
			verbose = 1;
			break;
		case 's':/*Armazena as sessoes em um arquivo*/
			sessiondebug = 1;
			break;
		case 'L':/*Latencia*/
			latencia = atoi(optarg);
			break;
		case -1:
			break;
		default:
			abort();
		}
	} while (next_option != -1);

	signal(SIGTERM, signal_handler);
	signal(SIGINT, signal_handler);

	startMongoose(options);
	printf("Escutando a(s) porta(s) %s com document root [%s]\n",
			mg_get_option(ctx, "listening_ports"),
			mg_get_option(ctx, "document_root"));
	while (exit_flag == 0) {
		sleep(1);
	}
	printf("Terminando com o sinal %d, esperando todos os threads terminarem.",
			exit_flag);
	fflush(stdout);
	mg_stop(ctx);
	printf("%s", " pronto.\n");
	return EXIT_SUCCESS;
}

