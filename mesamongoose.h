#ifndef __MESAMONGOOSE__
#define __MESAMONGOOSE__

#include "mongoose.h"

#define TAMDEFAULTBUFFER (128 * 1024)
#define DBTIMEOUT 10000
#define MAX_SESSIONS 2000
#define SESSION_TTL 60000 
#define MAX_USER_LEN 256
#define TAMBUFFERPARAMETROS (16 * 1024) 
#define FROM    "<sender@example.org>"
#define TO      "<addressee@example.net>"
#define CC      "<info@example.org>"


extern int verbose;
extern int sessiondebug;
#endif


